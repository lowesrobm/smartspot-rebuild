<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
	
	$app->get('/smartspot/c/{category}', function ( $request, $response, $args ) use ( $app, $pdo ) {

		return $response->withRedirect('/smartspot/products/'.$args['category']);
	
	});

	$app->get('/smartspot/products', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		$category = $pdo->select()
					->from( 'smarthome_categories' )
					->where( 'shortname', '=', 'all' )
					->execute()
					->fetch();
					
		$products = $pdo->select()
					->from( 'smarthome_products' )
					->orderBy( 'category_page_order', 'ASC' )
					->execute()
					->fetchAll();
	
		foreach( $products as $key => $product ):
			
			$product_rating = number_format($product['rating'], 2, '.', '');
			
			$products[$key]['rating_class'] = productRating( $product_rating );
			
		endforeach;
		
    	return $this->view->render( $response, '/category/global.category.twig', array( 'category' => $category, 'products' => $products,  'bodyclass' => 'cat' ) );
		
	});

	$app->get('/smartspot/products/{category}', function ( $request, $response, $args ) use ( $app, $pdo ) {
    	
    	$category = $pdo->select()
					->from( 'smarthome_categories' )
					->where( 'shortname', '=', $args['category'] )
					->execute()
					->fetch();
					
		$products = $pdo->select()
					->from( 'smarthome_products' )
					->where( 'product_category', '=', $args['category'] )
					->orWhere( 'additional_categories', 'LIKE', '%'.$args['category'].'%' )
					->orderBy( 'category_page_order', 'ASC' )
					->execute()
					->fetchAll();
	
		foreach( $products as $key => $product ):
			
	    	$product_rating = number_format($product['rating'], 2, '.', '');
			
			$product['rating_class'] = productRating( $product_rating );
			
		endforeach;

    	return $this->view->render( $response, '/category/global.category.twig', array( 'category' => $category, 'products' => $products,  'bodyclass' => 'cat' ) );
    	
    })->setName('category');
    
?>