<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    $app->get('/cms/users', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		$message = "";
		
		if( $_SESSION['messages']['auth_change'] != '' ):
			$message = $_SESSION['messages']['auth_change'];
			unset( $_SESSION['messages']['auth_change'] );
		endif;
		
		$users = $pdo->select()
					 ->from( 'liftoff_core_users' )
					 ->orderBy( 'permission_level', 'DESC' )
					 ->execute()
					 ->fetchAll();
						
        return $this->view->render( $response, '/cms/users.twig', array( 'auth' => $_SESSION['auth'], 'userlist' => $users, 'message' => $message ) );
    
    })->setName('users'); 
        
    $app->get('/cms/users/new', function ( $request, $response, $args ) use ( $app, $pdo ) {
								
        return $this->view->render( $response, '/cms/users-create.twig', array( 'auth' => $_SESSION['auth'] ) );
    
    })->setName('users-new');
        
    $app->post('/cms/create-user', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
        $data = $request->getParsedBody();
		
		$user_response = createNewUser( $data['username'], $data['fullname'], $data['email'], $data['permission'], $pdo );
								
        return $user_response;
    
    })->setName('users-new');
        
    $app->post('/cms/delete-user', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
        $data = $request->getParsedBody();
		
		$user_response = deleteUser( $data['id'], $pdo );
								
        return $user_response;
    
    })->setName('users-delete');
        
    $app->get('/cms/users/{authmethod}/{userid}', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		if( $args['authmethod'] == 'revokeauth' ):

			$pdo->update( array( 'authorized' => "no" ) )
                ->table( 'liftoff_core_users' )
                ->where( 'id', '=', $args['userid'] )
				->execute();

			$message = "Revoked authorization access for User ID: ".$args['userid'];
				
		elseif( $args['authmethod'] == 'grantauth' ):
	
			$pdo->update( array( 'authorized' => "yes" ) )
                ->table( 'liftoff_core_users' )
                ->where( 'id', '=', $args['userid'] )
				->execute();

			$message = "Granted authorization access for User ID: ".$args['userid'];
	
		endif;
		
		$_SESSION['messages']['auth_change'] = $message;
			
        return $response->withRedirect( '/cms/users' );
    
    })->setName('users-authorization-change'); 
    
?>