<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    $app->get('/cms/account-settings', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		$firstlogin = 'no';
		
		$user = $pdo->select()
					->from( 'liftoff_core_users' )
					->where( 'id', '=', $_SESSION['auth']['user_id'] )
					->execute()
					->fetch();
					
		if( $_SESSION['auth']['first_login'] == 'yes' || $user['first_login'] == 'yes' ):
			
			$firstlogin = 'yes';
			
			if ( isset( $_SESSION['auth']['first_login'] ) )
				unset( $_SESSION['auth']['first_login'] );
			
		endif;
					
        return $this->view->render( $response, '/cms/account-settings.twig', array( 'auth' => $_SESSION['auth'], 'user' => $user, 'firstlogin' => $firstlogin ) );
    
    })->setName('account-settings'); 
    
    $app->post('/cms/change-password/{newpassword}/{conpassword}', function ( $request, $response, $args ) use ( $app, $pdo ) {

        if ( $args['newpassword'] === $args['conpassword'] ):
        	
        	if ( strlen( $args['newpassword'] ) > 5 && strlen( $args['conpassword'] ) > 5 ):
        		
        		$newpassword = hash('sha256', $args['newpassword'] );
        		
        		$pdo->update( array( 'password' => $newpassword ) )
	                ->table( 'liftoff_core_users' )
	                ->where( 'id', '=', $_SESSION['user_id'] )
					->execute();
				
				return json_encode( array('type' => 'success', 'alertclass' => 'success', 'msg' => '<strong>Success:</strong> Your password has been updated. You will now be logged out.') );
							
        	else:
        	
        		return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> Passwords must be at least 6 characters long.') );
        	
        	endif;
        	
        else:
        
        	return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> Passwords do not match.') );
        
        endif;
    
    })->setName('change-password');
    
    $app->post('/cms/first-login-off', function ( $request, $response, $args ) use ( $app, $pdo ) {
    		
		$pdo->update( array( 'first_login' => 'no' ) )
			->table( 'liftoff_core_users' )
			->where( 'id', '=', $_SESSION['user_id'] )
			->execute();

    })->setName('change-password');  

?>