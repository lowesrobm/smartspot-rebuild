<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    $app->get('/cms/login', function ( $request, $response, $args ) use ( $app, $pdo ) {

		$auth_error_message = "";

		if( isset( $_SESSION['errors']['auth_msg'] ) ):
			$auth_error_message = $_SESSION['errors']['auth_msg'];
			
			unset( $_SESSION['errors']['auth_msg'] );
			
		endif;
		
        return $this->view->render( $response, '/cms/login.twig', array( 'auth' => $_SESSION['auth'], 'auth_error' => $auth_error_message ) );
    
    })->setName('login'); 
    
    $app->post('/cms/auth/login', function ( $request, $response, $args ) use ( $app, $pdo ) {

        $auth = $request->getParsedBody();
        
        $auth['password'] = hash( 'sha256', $auth['password'] );
        
        $user = $pdo->select()
					->from( 'liftoff_core_users' )
					->where( 'username', '=', $auth['user'] )
					->execute()
					->fetch();
					
		if( !empty( $user ) ):
			
			if ( $user['login_attempts'] < 5 ):
		
				if ( $user['authorized'] === 'yes' ):
				
					if ( $auth['password'] === $user['password'] ):
						
						$_SESSION['auth']['user_id'] = $user['id'];
						$_SESSION['auth']['username'] = $user['username'];
						$_SESSION['auth']['full_name'] = $user['fullname'];
						$_SESSION['auth']['email_address'] = $user['emailaddress'];
						$_SESSION['auth']['permission_level'] = $user['permission_level'];
						
						$pdo->update( array('last_login' => time(), 'login_attempts' => 0 ) )
			                ->table( 'liftoff_core_users' )
			                ->where( 'id', '=', $user['id'] )
							->execute();
						
						if( $user['first_login'] == 'no' ):
							return $response->withRedirect( '/cms' );
						else:
							$_SESSION['auth']['first_login'] = $user['first_login'];
							return $response->withRedirect( '/cms/account-settings' );
						endif;
					
					else:
					
						$_SESSION['errors']['auth_msg'] = "The password your entered does not match the password on the account.";
						
						$attempts = $user['login_attempts'] + 1;
						
						$pdo->update( array( 'login_attempts' => $attempts ) )
			                ->table( 'liftoff_core_users' )
			                ->where( 'id', '=', $user['id'] )
							->execute();
                
						return $response->withRedirect( '/cms/login' );
			                
					endif;
				
				else:
				
					$_SESSION['errors']['auth_msg'] = "You are not authorized to access this CMS.";
					
					$attempts = $user['login_attempts'] + 1;
					
					$pdo->update( array( 'login_attempts' => $attempts ) )
		                ->table( 'liftoff_core_users' )
		                ->where( 'id', '=', $user['id'] )
		                ->execute();
                
					return $response->withRedirect( '/cms/login' );
					
				endif;
			
			else:
			
				$_SESSION['errors']['auth_msg'] = "Too many login attempts. Your account is locked - please contact your manager.";
				
				$pdo->update( array( 'authorized' => 'no' ) )
	                ->table( 'liftoff_core_users' )
	                ->where( 'id', '=', $user['id'] )
	                ->execute();
                
				return $response->withRedirect( '/cms/login' );
			
			endif;
		
		else:
		
			$_SESSION['errors']['auth_msg'] = "Sorry, we couldn't find that user.";
                
            return $response->withRedirect( '/cms/login' );
			
		endif;
			
        
    })->setName('auth-login'); 
    
    $app->get('/cms/auth/logout', function ( $request, $response, $args ) use ( $app, $pdo ) {

        if( isset ($_SESSION['auth'] ) )
        	unset( $_SESSION['auth'] );
        	
        session_destroy();
        
        return $response->withRedirect( '/cms/login' );
    
    })->setName('logout');  
?>