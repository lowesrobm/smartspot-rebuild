<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    $app->get('/cms/sites', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		$sites = $pdo->select()
					 ->from( 'liftoff_core_sites' )
					 ->orderBy( 'site_name', 'ASC' )
					 ->execute()
					 ->fetchAll();
					 
        return $this->view->render( $response, '/cms/sites.twig', array( 'auth' => $_SESSION['auth'], 'sites' => $sites ) );
    
    })->setName('sites');  
        
    $app->get('/cms/sites/new', function ( $request, $response, $args ) use ( $app, $pdo ) {
					 
        return $this->view->render( $response, '/cms/sites-create.twig', array( 'auth' => $_SESSION['auth'] ) );
    
    })->setName('sites-new');  
        
    $app->post('/cms/create-site', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
        $data = $request->getParsedBody();
		
		$site_response = createNewSite( $data['sitename'], $data['description'], $data['status'], time(), $pdo );
								
        return $site_response;
    
    })->setName('sites-new');
        
    $app->get('/cms/sites/status-change/{status}/{id}', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		$site_response = setSiteStatus( $args['id'], $args['status'], $pdo );
							
        return $response->withRedirect( '/cms/sites' );
    
    })->setName('site-status');
    
?>