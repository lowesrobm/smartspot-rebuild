<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
 
    $app->get('/cms/products', function ( $request, $response, $args ) use ( $app, $pdo ) {

        return $this->view->render( $response, '/cms/products.twig', array( 'auth' => $_SESSION['auth'] ) );
    
    })->setName('products'); 
    
?>