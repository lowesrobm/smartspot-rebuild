<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
 
    $app->get('/cms/pages', function ( $request, $response, $args ) use ( $app, $pdo ) {

        return $this->view->render( $response, '/cms/pages.twig', array( 'auth' => $_SESSION['auth'] ) );
    
    })->setName('pages');
 
    $app->get('/cms/pages/new', function ( $request, $response, $args ) use ( $app, $pdo ) {

		$elements = $pdo->select()->from('liftoff_core_page_elements')->orderBy('element_order', 'ASC')->execute()->fetchAll();

        return $this->view->render( $response, '/cms/pages-create.twig', array( 'auth' => $_SESSION['auth'], 'elements' => $elements ) );
    
    })->setName('pages-new');
 
    $app->get('/cms/pages/creator-preview', function ( $request, $response, $args ) use ( $app, $pdo ) {
	    
        return $this->view->render( $response, '/partials/creator-preview.twig', array( 'auth' => $_SESSION['auth'], 'elements' => $elements ) );
    
    })->setName('pages-new-creator-preview');
    
?>