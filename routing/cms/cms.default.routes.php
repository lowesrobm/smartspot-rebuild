<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    $app->get('/cms', function ( $request, $response, $args ) use ( $app, $pdo ) {
		
		if ( empty( $_SESSION['auth'] ) )
			return $response->withRedirect( '/cms/login' );
		
		$smarthome = [];
		
		$smarthome_products = 	$pdo->select()
									->from( 'smarthome_products' )
									->whereMany( array( 'published' => 1 ), '=' )
									->orderBy( 'id', 'ASC' )
									->execute()
									->fetchAll();
		
		$smarthome_categories = $pdo->select()
									->from( 'smarthome_categories' )
									->orderBy( 'id', 'ASC' )
									->execute()
									->fetchAll();
								
		$smarthome['products'] = $smarthome_products;
		$smarthome['categories'] = $smarthome_categories;
		
		$lastuser = $pdo->select()
						->from( 'liftoff_core_users' )
						->orderBy( 'last_login', 'DESC' )
						->limit(1,0)
						->execute()
						->fetch();
	
        return $this->view->render( $response, '/cms/dashboard.twig', array( 'auth' => $_SESSION['auth'], 'smarthome_data' => $smarthome, 'lastlogin' => $lastuser ) );
    
    })->setName('dashboard'); 
    
?>