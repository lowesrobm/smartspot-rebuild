<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
        
    $app->get('/smartspot', function ( $request, $response, $args ) use ( $app, $pdo ) {

			$house_carousel_products = [];
	
			$house_products = 	$pdo->select()
									->from( 'smarthome_products' )
									->whereMany( array( 'published' => 1, 'homepage_house' => 1 ), '=' )
									->orderBy( 'house_product_order', 'ASC' )
									->execute()
									->fetchAll();
			
			foreach( $house_products as $product )
				$house_carousel_products[$product['product_category']][] = $product;
			
	        return $this->view->render( $response, '/home/index.twig', array( 'house_carousel_products' => $house_carousel_products, 'bodyclass' => 'home' ) );
    
    })->setName('homepage'); 
    
    $app->get('/smartspot/getlocation/{query}', function( $request, $response, $args ) use ( $app ) {
	    
		if( isset( $args['query'] ) && !empty( $args['query'] ) )
		    $data = zipInToStore( $args['query'] );
		
		if( isset( $data ) && !empty ( $data ) )
			return $data;
	    
    });
    
    $app->get('/smartspot/uses', function( $request, $response, $args ) use ( $app ) {
	    
		return $this->view->render( $response, '/home/index.twig');
	    
    });
    
    $app->get('/smartspot/smarthouse', function( $request, $response, $args ) use ( $app ) {
	    
		return $this->view->render( $response, '/home/index.twig');
	    
    });
    
    $app->get('/smartspot/set-temporary-store/{store_name}/{store_number}/{time_open}/{time_close}/{street_address}/{city}/{zip}/{phone}/{distance}', function( $request, $response, $args ) use ( $app ) {
	    
			if (session_status() == PHP_SESSION_ACTIVE):
				
				$_SESSION['storeName'] 		= $args['storeName'];
		    $_SESSION['storeNumber'] 	= $args['store_number'];
		    $_SESSION['storeOpen'] 		= $args['time_open'];
		    $_SESSION['storeClose'] 	= $args['time_close'];
		    
		    $_SESSION['storeStreet'] 	= $args['street_address'];
		    $_SESSION['storeCity'] 		= $args['city'];
		    $_SESSION['storeZip'] 		= $args['zip'];
				$_SESSION['storePhone'] 	= $args['phone'];
		    
		    $_SESSION['storeDistance'] = $args['distance'];
		    
		    return true;
		    
	    else:
	    
	    	return false;
	    
			endif;
    });
    
    $app->get('/smartspot/check-store-in-session', function( $request, $response ) use ( $app ) {
	    
			if( isset( $_SESSION['storeNumber'] ) && $_SESSION['storeNumber'] !== '' )
				return true;
			else
				return false;
	    
    });

?>