<?php
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

	$app->get('/smartspot/products/detail/{id:[0-9]+}', function ( $request, $response, $args ) use ( $app, $pdo ) {
    	
  	$product = $pdo->select()
		->from( 'smarthome_products' )
		->where( 'product_id', '=', $args['id'] )
		->execute()
		->fetch();
    	
  	if( !empty( $product['additional_images'] ) && $product['additional_images'] !== '' )
  		$additional_images = explode( ',', $product['additional_images'] );
  	
  	$product_rating = number_format($product['rating'], 2, '.', '');
		$product['rating_class'] = productRating( $product_rating );
		
		
		$features = $pdo->select()
		->from( 'smarthome_products_features' )
		->where( 'product_guid', '=', $args['id'] )
		->execute()
		->fetch();
				  
		$appdetails = $pdo->select()
		->from( 'smarthome_products_apps' )
		->where( 'product_guid', '=', $args['id'] )
		->execute()
		->fetch();
				  
		$featcarousel = $pdo->select()
		->from( 'smarthome_products_features_carousel' )
		->whereMany( array( 'product_guid' => $args['id'], 'published' => 'yes' ), '=' )
		->orderBy('slide_order', 'ASC')
		->execute()
		->fetchAll();
				  
		$specs = $pdo->select()
		->from( 'smarthome_products_specs' )
		->whereMany( array( 'product_guid' => $args['id'], 'published' => 'yes' ), '=' )
		->orderBy('key_order', 'ASC')
		->execute()
		->fetchAll();
						
		$incategory	= $pdo->select()
		->from( 'smarthome_products' )
		->whereMany( array( 'product_category' => $product['product_category'] ), '=' )
		->whereMany( array( 'product_id' => $product['product_id'] ), '!=' )
		->orderBy('category_page_order', 'ASC')
		->execute()
		->fetchAll();
					
    return $this->view->render( $response, '/product/global.product.twig', 
						    								array( 
						    									'product' 			=> $product,
						    									'products' 			=> $incategory, 
						    									'additional_images' => $additional_images, 
						    									'features' 			=> $features,
						    									'appdetails' 		=> $appdetails, 
						    									'featurecarousel' 	=> $featcarousel,
						    									'specs' 			=> $specs,
						    									'bodyclass' 		=> 'pdp' 
						    								) 
															);
    	
    })->setName('pdp');	
   

	$app->get('/smartspot/fetch-product-quickview/{id:[0-9]+}', function ( $request, $response, $args ) use ( $app, $pdo ) {
    	
  	$product = 	$pdo->select()
		->from( 'smarthome_products' )
		->where( 'product_id', '=', $args['id'] )
		->execute()
		->fetch();
    	
  	if( !empty( $product['additional_images'] ) && $product['additional_images'] !== '' )
  		$product['additional_images'] = explode( ',', $product['additional_images'] );
  	
  	$product_rating = number_format($product['rating'], 2, '.', '');
		$product['rating_class'] = productRating( $product_rating );
  	
  	return json_encode( $product );
    	
  })->setName('quickview-fetch');	

	$app->post('/smartspot/add-to-cart', function ( $request, $response, $args ) use ( $app, $pdo ) {
    	
    $data = $request->getParsedBody();
    	
  	if( isset( $_SESSION['cart'] ) ):
  	
  		if( isset( $_SESSION['cart'][$data['product_id']] ) ):
  		
  			$current_quantity = $_SESSION['cart'][$data['product_id']]['quantity'];
  			
  			if( $data['method'] == 'add'):
  			
  				$_SESSION['cart'][$data['product_id']]['quantity'] = $current_quantity + 1;
  			
  			elseif( $data['method'] == 'subtract' ):
  				
  				if( $current_quantity == 1 ):
  				
  					unset( $_SESSION['cart'][$data['product_id']] );
  				
  				else:	
  				
  					$_SESSION['cart'][$data['product_id']]['quantity'] = $current_quantity - 1;
			
				endif;
			
			endif;
			
  		else:
  		
  			$_SESSION['cart'][$data['product_id']]['product_id'] = $data['product_id'];
  			$_SESSION['cart'][$data['product_id']]['product_title'] = $data['product_title'];
  			$_SESSION['cart'][$data['product_id']]['product_picture'] = $data['product_picture'];
				$_SESSION['cart'][$data['product_id']]['single_price'] = $data['price'];
  			$_SESSION['cart'][$data['product_id']]['quantity'] = 1;
  		
  		endif;
  	
  	else:
  		
		$_SESSION['cart'][$data['product_id']]['product_id'] = $data['product_id'];
		$_SESSION['cart'][$data['product_id']]['product_title'] = $data['product_title'];
		$_SESSION['cart'][$data['product_id']]['product_picture'] = $data['product_picture'];
		$_SESSION['cart'][$data['product_id']]['single_price'] = $data['price'];
		$_SESSION['cart'][$data['product_id']]['quantity'] = 1;
  	
  	endif;
  	
  	return json_encode( $_SESSION['cart'][$data['product_id']] );
  	
  })->setName('addtocart');	
						
    
?>