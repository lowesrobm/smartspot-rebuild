function updatePassword(thisid, firstlogin) {
	
	var currentText = $(thisid).html();
	$(thisid).html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>');

	var newpassword = $('#newpassword').val();
	var conpassword = $('#conpassword').val();
	
	if( newpassword == '' || conpassword == '' ) {
		$("#msgcontainer").html('');
		$("#msgcontainer").addClass('show');
		$("<div class='alert alert-danger'><span><strong>Error - </strong> One of the password fields is blank.</span></div>").appendTo( $("#msgcontainer") );
		setTimeout(function(){ $("#msgcontainer").removeClass('show'); }, 5000);
		return false;
	}	
	
	$.ajax({
	    type: 'POST',
	    url: '/cms/change-password/'+newpassword+'/'+conpassword,
	    success: function( response ) {
	        if( response ) {
		        var message = JSON.parse( response )
		        
				$("#msgcontainer").html('');
		        $("#msgcontainer").addClass('show');
				$("<div class='alert alert-"+message.alertclass+"'><span>"+message.msg+"</span></div>").appendTo( $("#msgcontainer") );
				setTimeout(function(){ $("#msgcontainer").removeClass('show'); window.location.href = '/cms/auth/logout'; }, 2500);
			}
	    }
	});
	
	if ( firstlogin == 1 ){
		$.ajax({
		    type: 'POST',
		    url: '/cms/first-login-off'
		});
	}
	
	$(thisid).html(currentText);
}

function createNewUser() {
	var username = $('#username').val();
	var fullname = $('#fullname').val();
	var email = $('#email').val();
	var permissionlevel = $('#permissionlevel').val();
	
	$.ajax({
	    type: 'POST',
	    url: '/cms/create-user',
	    data: { username: username, fullname: fullname, email: email, permission: permissionlevel },
	    success: function( response ) {
	        if( response ) {
		        var message = JSON.parse( response )
		        
				$("#msgcontainer").html('');
		        $("#msgcontainer").addClass('show');
				$("<div class='alert alert-"+message.alertclass+"'><span>"+message.msg+"</span></div>").appendTo( $("#msgcontainer") );
				setTimeout(function(){ 
					$("#msgcontainer").removeClass('show'); 
					if(message.type == 'success') {
						window.location.href = '/cms/users';
					}
				}, 2500);
				
				
			}
	    }
	});
}

function deleteUser(userid) {
	
	if ( confirm("This will permanently delete the user from the system, are you sure?") == true) {
	
		$.ajax({
		    type: 'POST',
		    url: '/cms/delete-user',
		    data: { id: userid },
		    success: function( response ) {
		        if( response ) {
			        var message = JSON.parse( response )
			        
					$("#msgcontainer").html('');
			        $("#msgcontainer").addClass('show');
					$("<div class='alert alert-"+message.alertclass+"'><span>"+message.msg+"</span></div>").appendTo( $("#msgcontainer") );
					setTimeout(function(){ 
						$("#msgcontainer").removeClass('show'); 
						if(message.type == 'success') {
							$('#user_row_'+userid).remove();
						}
					}, 2500);
					
					
				}
		    }
		});
	
	} else {
		return false;
	}
}

function createNewSite() {
	var sitename = $('#sitename').val();
	var description = $('#description').val();
	var status = $('#status').val();
	
	$.ajax({
	    type: 'POST',
	    url: '/cms/create-site',
	    data: { sitename: sitename, description: description, status: status },
	    success: function( response ) {
	        if( response ) {
		        var message = JSON.parse( response )
		        
				$("#msgcontainer").html('');
		        $("#msgcontainer").addClass('show');
				$("<div class='alert alert-"+message.alertclass+"'><span>"+message.msg+"</span></div>").appendTo( $("#msgcontainer") );
				setTimeout(function(){ 
					$("#msgcontainer").removeClass('show'); 
					if(message.type == 'success') {
						window.location.href = '/cms/sites';
					}
				}, 2500);
				
				
			}
	    }
	});
}