$(document).ready(function() {

	var hero = $('.hero-carousel'),
		category = $('.category-carousel'),
		houseProductCarouselSecurity = $('.house-product-carousel-security'), 
		houseProductCarouselEnergy = $('.house-product-carousel-energy'),
		houseProductCarouselComfort = $('.house-product-carousel-comfort'),
		pdpThumbnails = $('.pdp-thumbnail-carousel'),
		qvThumbnails = $('.quickview-thumbnail-carousel'),
		pdpPageImages = $('.pdp-images-carousel');
		
	hero.owlCarousel({
	    items: 1,
	    loop: true,
	    margin: 0,
	    autoplay: false,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true,
	    autoplaySpeed: 1000,
	    navSpeed: 1000,
	    dotsSpeed: 1000,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true,
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    smartSpeed: 450
	});
	
	/*
	hero.on('changed.owl.carousel', function(event) {
	    $('#hero-text-1').css('opacity', 1);
	});
	*/
	
	category.owlCarousel({
	    items: 1,
	    loop: true,
	    margin: 0,
	    autoplay: true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true,
	    autoplaySpeed: 1000,
	    navSpeed: 1000,
	    dotsSpeed: 1000,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true,
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    smartSpeed: 450
	});
	
	pdpThumbnails.owlCarousel({
	    items: 5,
	    loop: true,
	    margin: 0,
	    autoplay: false,
	    navSpeed: 1000,
	    dotsSpeed: 1000,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true,
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    smartSpeed: 450
	});
	
	qvThumbnails.owlCarousel({
	    items: 5,
	    loop: true,
	    margin: 10,
	    autoplay: false,
	    navSpeed: 1000,
	    dotsSpeed: 1000,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true,
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    smartSpeed: 450
	});
	
	houseProductCarouselSecurity.owlCarousel({
	    items: 1,
	    loop: false,
	    margin: 50,
	    autoplay: false,
	    navSpeed: 1000,
	    dotsSpeed: 350,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true
	});
	
	houseProductCarouselEnergy.owlCarousel({
	    items: 1,
	    loop: false,
	    margin: 0,
	    autoplay: false,
	    navSpeed: 1000,
	    dotsSpeed: 350,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true
	});
	
	houseProductCarouselComfort.owlCarousel({
	    items: 1,
	    loop: false,
	    margin: 0,
	    autoplay: false,
	    navSpeed: 1000,
	    dotsSpeed: 350,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true
	});
		
	pdpPageImages.owlCarousel({
	    items: 1,
	    loop: true,
	    margin: 10,
	    autoplay: true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true,
	    autoplaySpeed: 1000,
	    navSpeed: 1000,
	    dotsSpeed: 1000,
	    dragEndSpeed: 1000,
	    touchDrag: true,
	    dots: true,
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    smartSpeed: 450
	});

	$('.parallax-window').parallax( { speed: 0.5 } );
});
/*
$(document).ready( function() {

$(".house-icon").rippleSauce({ color: '#E3263E', size: 120, time: 600, borderWidth: 2 });
$(".house-icon").rippleSauce({ color: '#E3263E', size: 110, time: 650, borderWidth: 2 });
$(".house-icon").rippleSauce({ color: '#E3263E', size: 100, time: 700, borderWidth: 2 });
	setInterval( $(".house-icon").click(), 5000);
});
*/

$(document).ready( function () { $( '.lazyload' ).css('display', 'none'); });

function setActiveHouseCategory( category ) {
	$( '.house-categories li a' ).removeClass('active');
	$( '#house-category-' + category ).addClass('active');
	
	$( '.hpc' ).css('display', 'none');
	$( '#house-product-carousel-' + category ).css('display', 'block');
	
	$( '.house-icon' ).hide();
	$( '.house-icon.'+category ).show();
}


$('.pdp-thumb-navigation.down').on('click', function() {
	$('.pdp-thumb-container').delay(10).animate({
        scrollTop: $('.pdp-thumb-container').offset().top 
    }, 300);
});

$('.pdp-thumb-navigation.up').on('click', function() {
	$('.pdp-thumb-container').delay(10).animate({
        scrollTop: 0 
    }, 300);
});

function timeTo12HrFormat(time)
{   // Take a time in 24 hour format and format it in 12 hour format
    var time_part_array = time.split(":");
    var ampm = 'AM';

    if (time_part_array[0] >= 12) {
        ampm = 'PM';
    }

    if (time_part_array[0] > 12) {
        time_part_array[0] = time_part_array[0] - 12;
    }

    formatted_time = time_part_array[0] + ':' + time_part_array[1] + ' ' + ampm;

    return formatted_time;
}

function ZipIn( query = '' ) {
		
    if ( $("#locale_query").value !== '' && $("#locale_query").value !== 'undefined' && $("#locale_query").value !== null )
    {
	    var query = $("#locale_query").val();
    }
	
	if ( query !== '' ) {
		
		$.ajax({
	        type: 'GET',
	        url: '/smartspot/getlocation/' + query,
	        success: function( response ){
		        if ( response !== false )
	            {
		            var data = JSON.parse( response );
		            
					$("#zipin_store_list").html('');
					
					var d = new Date();
					var weekday = new Array(7);
					weekday[0] = "sunday";
					weekday[1] = "monday";
					weekday[2] = "tuesday";
					weekday[3] = "wednesday";
					weekday[4] = "thursday";
					weekday[5] = "friday";
					weekday[6] = "saturday";
					
					var today = weekday[d.getDay()];
										
					var times = new Array(2);
					
					$.each( data, function( i, o ) {
						$.each( o, function( i, store ) {
							$.each( store.dailyHours, function(day, hours) {
								if (today === day ) {
									var openfrom = new Date(1000 * hours[0]).toISOString().substr(11, 5);
									var opentill = new Date(1000 * hours[1]).toISOString().substr(11, 5);
									
									times[0] = timeTo12HrFormat(openfrom);
									times[1] = timeTo12HrFormat(opentill);
								}
							});
							
							var directionslink = "https://www.google.com/maps?saddr=My+Location&daddr="+store.address1+"+"+store.city+"+"+store.state+"+"+store.zip;
														
							$("<li><h4 class='store-name'>"+store.storeName+"</h4><address class='store-address'>"+store.address1+"<br />"+store.city+"<br />"+store.state+", "+store.zip+"</address><p class='store-mini-details'>"+store.milesToStore+"m away. Open today from " + times[0] + " to " + times[1] + "</p><div class='pull-right store-actions'><a href='#' onclick=\"setStore( '"+store.storeName.replace(/'/g, "\\'")+"', '"+store.address1.replace(/'/g, "\\'")+"','"+store.city+"', '"+store.state+"', '"+store.zip+"', '"+store.milesToStore+"', '"+times[0]+"', '"+times[1]+"', '"+store.storeNumber+"', '"+store.phone+"' );\"  data-dismiss='modal' class='btn btn-comfort comfort-bg'>shop this store</a><br /><br /><a href='"+directionslink+"' class='btn btn-comfort btn-outline hint--top' aria-label='Need directions from your location to our store?' target='_blank'>get directions</a></div><hr class='store-separator' /></li>").appendTo("#zipin_store_list");
						});
					});

	            }
	        }
	    });
	}
	
}
function autoZipIn( query ) {
		
	if ( query !== '' ) {
		query = "28031";
		$.ajax({
	        type: 'GET',
	        url: '/smartspot/getlocation/' + query,
	        success: function( response ){
		        if ( response !== false )
	            {
		            var data = JSON.parse( response );
		            					
					var d = new Date();
					var weekday = new Array(7);
					weekday[0] = "sunday";
					weekday[1] = "monday";
					weekday[2] = "tuesday";
					weekday[3] = "wednesday";
					weekday[4] = "thursday";
					weekday[5] = "friday";
					weekday[6] = "saturday";
					
					var today = weekday[d.getDay()];
										
					var times = new Array(2);
					
					$.each( data, function( i, o ) {
						$.each( o, function( i, store ) {
							$.each( store.dailyHours, function(day, hours) {
								if (today === day ) {
									var openfrom = new Date(1000 * hours[0]).toISOString().substr(11, 5);
									var opentill = new Date(1000 * hours[1]).toISOString().substr(11, 5);
									
									times[0] = timeTo12HrFormat(openfrom);
									times[1] = timeTo12HrFormat(opentill);
								}
							});
							
							setStore( store.storeName, store.address1, store.city, store.state, store.zip, store.milesToStore, times[0], times[1], store.storeNumber, store.phone );
							
						});
					});
					return true;

	            }
	            else if ( response == false )
				{
					return false;
				}
	        }
	    });
	}
	
}

function setStore( storeName, street_address, city, state, zip, distance, time_open, time_close, store_number, phone ) {
	if (typeof(Storage) !== "undefined") {
	    
	    localStorage.setItem("storeName", storeName);
	    localStorage.setItem("storeNumber", store_number);
	    localStorage.setItem("storeOpen", time_open);
	    localStorage.setItem("storeClose", time_close);
	    
	    localStorage.setItem("storeStreet", street_address);
	    localStorage.setItem("storeCity", city);
	    localStorage.setItem("storeState", state);
	    localStorage.setItem("storeZip", zip);
	    localStorage.setItem("storePhone", phone);
	    
	    localStorage.setItem("storeDistance", distance);
	    
	    $('#storeLocation').html( city + ", " + state );
	    $('#storeOpenTill').html( "Open until " + time_close + " <i class='ion-chevron-down'></i>" );
	    
	} else {
	    console.log( "Couldn't write to local storage. Capturing information in PSR Flash Session, this will expire when the user leaves the website." );
	    
	    $.ajax({
	        type: 'GET',
	        url: '/smartspot/set-temporary-store/' + storeName + '/' + store_number + '/' + time_open + '/' + time_close + '/' + street_address + '/' + city + '/' + zip + '/' + phone + '/' + distance,
	        success: function( response ){
		        if (response == true) 
		        	console.log( "The information has been flashed to session." );
		        else
		        	console.log( "Couldn't flash the information to session, default store is being used." );
	        }
	    });
	}
}

$(document).ready( function () {
	
	if( localStorage.getItem("storeNumber") !== null ) {
		$('body').addClass('storeSetInLocalStorage');
		$('#storeLocation').html( localStorage.getItem("storeCity") + ", " + localStorage.getItem("storeState") );
		$('#storeOpenTill').html( "Open until " + localStorage.getItem("storeClose") + " <i class='ion-chevron-down'></i>" );
	}
	else 
	{
	
		var client_location, store_is_set_in_session, successful;
	
		$.ajax({
	        type: 'GET',
	        url: '/smartspot/check-store-in-session',
	        success: function( response ){
		        if( response == false ) {
			        if( localStorage.getItem("storeNumber") === null ) {
						$.getJSON('//freegeoip.net/json/?callback=?', function( data ) {
							
							client_location = JSON.parse( JSON.stringify( data, null, 2 ) );
									
							if( client_location.zip_code != '' ) {
								autoZipIn( client_location.zip_code );
							}
						});
					}
				}
	        }
	    });
	}
});

function clearLocalStorage() {
	$('body').removeClass('storeSetInLocalStorage');
	localStorage.clear();
	$('#storeLocation').html( '' );
	$('#storeOpenTill').html( "Set store <i class='ion-chevron-down'></i>" );
}


$(document).ready( function() { 
	$(".product-expansion").height( $(".panel-height").height() );
});

$(window).resize(function(){
	$(".product-expansion").height( $(".panel-height").height() );
});

function changeVariant( product_id ) {
	var current_productid = product_id;
}

function quickview( product_id ) {
	
	$(".quickview-loading").show();
	$(".quickview-preload-content").hide();
	
	$.ajax({
	    type: 'GET',
	    url: '/smartspot/fetch-product-quickview/'+product_id,
	    success: function( response ) {
	        if( response ) {
		        var data = JSON.parse( response )
		        
		        var network_price = parseFloat(data.network_price);
		        
		        $("#quickview_thumbnails").html('');
		        $("#quickview_mainimage").html('');
		        
		        $("#quickview_productid").val( product_id );
		        $("#quickview_title").html( data.product_title );
		        $("#quickview_price").html( "$" + network_price.toFixed(2) );
		        $("#quickview_price_val").val( network_price.toFixed(2) );
		        
		        $("#quickview_description p").html( data.product_abstract );
		        $("#quickview_rating").html( "<div><span class='stars-container stars-"+data.rating_class+"'>★★★★★</span> <span class='review-count'>("+data.review_count+" reviews)</span></div>" );
		        $("#quickview_itemnum p").html( "<strong>Item #:</strong> " + data.item_number );
		        $("#quickview_modelnum p").html( "<strong>Model #:</strong> " + data.model_number );
		        
		        $("<img src='"+data.image_lg+"' class='qvmi' />").appendTo( $("#quickview_mainimage") );
		        
		        $("<div class='thumb'><img src='"+data.image_lg+"' onclick=\"quickviewChangeImage('"+data.image_lg+"');\" /></div>").appendTo( $("#quickview_thumbnails") );
		        
		        $.each(data.additional_images, function (index, value) {
					$("<div class='thumb'><img src='"+value+"' onclick=\"quickviewChangeImage('"+value+"');\"/></div>").appendTo( $("#quickview_thumbnails") );
				});
				
				$(".quickview-loading").hide();
				$(".quickview-preload-content").fadeIn('fast');
			}
	    }
	});
}

function quickviewChangeImage( img_url ) {
	$("#quickview_mainimage img.qvmi").attr('src', img_url);
}

$(document).ready( function() {
	$("#cartbutton").click( function( event ) {
	    event.preventDefault();
	    $(".cartpane").toggleClass("cart-open"); 
	    $(".cart-blackout").toggleClass("cart-open"); 
	});
	
	$(".cart-blackout").click( function( event ) {
	    event.preventDefault();
	    $(".cartpane").toggleClass("cart-open"); 
	    $(".cart-blackout").toggleClass("cart-open"); 
	});
});

function changeCart( product_id, price, addorsubtract, picture, title ) {
	
	if ( product_id == 'dynamic') {
		product_id 	= $("#quickview_productid").val();
		price 		= $("#quickview_price_val").val();
	}
	
	$.ajax({
        type: 'POST',
	    data: { product_id: product_id, price: price, method: addorsubtract, product_picture: picture, product_title: title },
        url: '/smartspot/add-to-cart',
        success: function( response ){
	        
	        var productdata = JSON.parse( response )
	        
	        if ( productdata.quantity > 0 ) {
		        $("#currentProductCount_"+product_id).html( productdata.quantity );
	        }
	        else
	        {
		        $('<li><div class="product-in-cart"><div class="col-xs-6"><div class="in-cart-product-image"><img src="'+product_picture+'" class="img-responsive" /></div></div><div class="col-xs-6"><p class="title">'+productdata.product_title+'</p><p class="price">$'+productdata.single_price+'</p><p class="quantity"><a href="javascript:;" onclick="changeCart( '+product_id+', '+price+', "subtract", "'+productdata.product_picture+'", "'+productdata.product_title+'" )" class="decrement"><i class="ion-minus-round"></i></a><span class="current-product-count" id="currentProductCount_'+product_id+'">'+productdata.quantity+'</span><a href="javascript:;" onclick="changeCart( '+product_id+', '+price+', "add", "'+productdata.product_picture+'", "'+productdata.product_title+'" )" class="increment"><i class="ion-plus-round"></i></a><a href="javascript:;" class="remove-product" id="removeproduct_'+product_id+'">Remove</a></p></div></div></li>').appendTo( $("#productsInCart") );
		    }
	        
	        if( productdata.quantity > 1 ) {
		        $("#removeproduct_"+product_id).css("display", "none");
	        }
	        
	        console.log(response);
        }
    });
	
}