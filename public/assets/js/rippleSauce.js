
(function( $ ) {
	'use strict';

	$.fn.rippleSauce = function(opt) {

		var options = $.extend({ color: '#000', size: 20, time: 500, borderWidth: 2 }, opt);

		var id = 0;

		$(this).on('click', function(e) {
			var x = e.clientX;
			var y = $(window).scrollTop() + e.clientY;
			$('<div>')
			.attr('id', 'rippleSauce_' + id++)
			.css({
				'width': 0,
				'height': 0,
				'border': options.borderWidth + 'px solid ' + options.color,
				'position': 'absolute',
				'left': x,
				'top': y,
				'border-radius': '50%'
			})
			.animate({
				'width': options.size + 'px',
				'height': options.size + 'px',
				'left': x - 0.5 * options.size,
				'top': y - 0.5 * options.size,
				opacity: 0
			}, options.time, function() {
				$(this).remove();
			})
			.appendTo('body');
		});

		return this;

	}

}( jQuery ));