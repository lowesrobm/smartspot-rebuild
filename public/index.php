<?php
    date_default_timezone_set('America/New_York');
	
	session_set_cookie_params('1209600'); //Keep sessions for 14 days.
	
	if ( session_status() !== PHP_SESSION_ACTIVE )
		session_start();
	
	//unset($_SESSION['cart']);
	
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
	
	//Require the basics.
    require '../vendor/autoload.php';
    require '../dependencies/config.php';
	
	//Instantiate app with desired configuration settings.
    $app = new \Slim\App( ["settings" => $config] );
	
	$pdo = new \Slim\PDO\Database($config['settings']['db']['dsn'], $config['settings']['db']['username'], $config['settings']['db']['password']);
	
    $container = $app->getContainer();
    
    //System flash messages
    $container['flash'] = function () {
	    return new \Slim\Flash\Messages();
	};
    
    //Instantiate view using Twig Template Engine
    $container['view'] = function ($container) {
	    $view = new \Slim\Views\Twig('templates', [
		    'debug' => true,
		    //'cache' => '/tmp'
	    ]);
	    
	    $view->addExtension( new \Slim\Views\TwigExtension(
	        $container['router'],
	        $container['request']->getUri()
	    ));

	    $view->addExtension( new Twig_Extension_Debug() );
		
		$view['current_url'] 	= $_SERVER['REQUEST_URI'];
		$view['base_url']		= "http://".$_SERVER['HTTP_HOST'];
		$view['asset_url']		= "http://".$_SERVER['HTTP_HOST']."/assets";

		if ( isset( $_SESSION['cart'] ) )
			$view['cart'] 		= $_SESSION['cart'];
				
	    return $view;
	};	
	/************************
	 *		PRODUCTS		*
	 ************************/

	include '../middleware/products.php';
	
	$last_update_threshold = strtotime('today midnight');
	
	$stored_products = $pdo->select()->from('smarthome_products')->where('last_update', '<', $last_update_threshold)->execute()->fetchAll();
	
	
	foreach($stored_products as $to_update):
		
		$ch = curl_init();
			
		$header = array();
		$header[] = 'Content-Language: en-US';
		$header[] = 'Content-Type: application/json';
		$header[] = 'Authorization: Basic QWRvYmU6ZW9pdWV3ZjA5ZmV3bw==';
		
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
		curl_setopt( $ch, CURLOPT_URL, "http://api.lowes.com/product/nvalue?nValue=2731634481&storeNumber=0999&priceFlag=status&api_key=2jnb6kzs956d6d92mhe9fhxj&showEpc=1&showReviews=1" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_TCP_KEEPALIVE, true );
		curl_setopt( $ch, CURLOPT_TCP_KEEPIDLE, 2 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		
		$receive = curl_exec( $ch );
		
		if( curl_errno( $ch ) )
			throw new Exception( curl_error( $ch ) );
		
		curl_close( $ch );
		
		$json = json_decode( $receive );
		
		foreach( $json->productList as $product ):
		
			if( !empty( $product->epc->AdditionalImages ) ):
				
				$object = json_decode( json_encode( $product->epc->AdditionalImages ), true );
	
				foreach( $object as $add_img ):
				
					$additional_images = implode( ',', str_replace('//', 'https://', $add_img ) );
				
				endforeach;
				
			endif;
			
			if( isset( $product->rating ) && $product->rating !== '' )
				$rating = $product->rating;
			
			if( isset( $product->reviewCount ) && $product->reviewCount !== '' )
				$reviewCount = $product->reviewCount;
			
			
			$pdo->update()
				->set(
					array(
						'item_number'			=> $product->itemNumber,
						'model_number'			=> $product->modelId,
						'product_brand' 		=> $product->brand,
						'product_description' 	=> $product->description,
						'network_price'			=> $product->networkPrice,
						'image_lg'				=> $product->imageUrls->lg,
						'image_xl'				=> $product->imageUrls->xl,
						'rating'				=> $rating,
						'review_count'			=> $reviewCount,
						'buyable'				=> $product->buyable,
						'published'				=> $product->published,
						'last_update'			=> time(),
						'additional_images'		=> $additional_images
					)
				)
				->table( 'smarthome_products' )
				->where( 'product_id', '=', $product->productId )
				->execute();
		
		endforeach;
		
	endforeach;		

	/************************
	 *		END PRODUCTS	*
	 ************************/
	
	include '../middleware/functions.php';
	
	require '../dependencies/router.php';
    
    $app->run();

?>