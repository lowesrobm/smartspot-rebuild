<?php
	
/*
	foreach( glob("./products/*.php") as $product)
		include( $product );
*/
	

	$product_array = array(
		array( 'security', 999930276 ), //Leeo
		array( 'security', 50241011 ), //Kevo
		array( 'security', 1000100029 ), //Arlo
		array( 'security', 1000098184 ), //Nest Outdoor Cam
		array( 'security', 1000141773 ), //Ring Stickup Cam
		array( 'security', 999908195 ), //Chamberlain
		array( 'security', 1000142909 ), //Ring Doorbell Pro
		array( 'energy', 999969270 ), //Ecobee
		array( 'energy', 999957973 ), //Lightify
		array( 'energy', 1000125089 ), //Nest Copper
		array( 'energy', 1000135207 ), //Iris
		array( 'energy', 999959769 ), //Lutron
		array( 'comfort', 1000178395 ), //Google Home
		array( 'comfort', 1000135231 ), //Tile Mate
		array( 'comfort', 1000147027 ), //Click and Grow
	);
/*	
	
	$products = array(
		
		'security' => array( 'category_name' 	=> 'security',
							 'product_ids'		=>  array(
									 					//Leeo
														'id' => 999930276, 
														'compatibilityImg' => "", 
														'compatibility' => []
													),
													array(
														//Netgear Arlo
														'id' => 1000100029, 
														'compatibilityImg' => "", 
														'compatibility' => []
													),
													array(
														//Nest Outdoor Cam
														'id' => 1000098184, 
														'compatibilityImg' => "/img/NestCam/Compatible-Items.png", 
														'compatibility' => array(
															'id' => 999908195,
															'name' => 'Chamberlain MyQ Garage Door Controller',
															'desc' => 'Control and monitor your garage door from anywhere with your smartphone.',
															'href' => "/smartspot/pd/999908195"
														)
													),
													array(
														//Ring Stickup Cam
														'id' => 1000141773, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Chamberlain
														'id' => 999908195, 
														'compatibilityImg' => "/img/Chamberlain/Compatible-Items.png", 
														'compatibility' => array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "A learning thermostat that can adjust temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)
													),
													array(
														//Ring Doorbell Pro
														'id' => 1000142909, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													)
							),
		'energy' => array(  'category_name' 	=> 'energy',
							'product_ids'		=>  array(
														//Ecobee
														'id' => 999969270, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Sylvania Lightify
														'id' => 999957973, 
														'compatibilityImg' => "/img/SylvaniaLightify/Compatible-Items.png", 
														'compatibility' => array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "A learning thermostat that can adjust temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)
													),
													array(
														//Nest Copper
														'id' => 1000125089, 
														'compatibilityImg' => "/img/NestCopper/Compatible-Items.jpg", 
														'compatibility' => array(
															'id' => 999959769,
															'name' => "Lutron Caseta Home Automation Smart Kit",
															'desc' => "Control your lights, shades, and temperature from anywhere with your smartphone.",
															'href' => "/smartspot/pd/999959769"
														),
														array(
															'id' => 999908195,
															'name' => 'Chamberlain MyQ Garage Door Controller',
															'desc' => 'Control and monitor your garage door from anywhere with your smartphone.',
															'href' => "/smartspot/pd/999908195"
														),
														array(
															'id' => 50241011,
															'name' => "Kwikset Kevo SmartKey Deadbolt",
															'desc' => "Bluetooth-enabled deadbolt provides enhanced home security and keyless access right from your smartphone.",
															'href' => "/smartspot/pd/50241011"
														)
													),
													array(
														//Iris Switch
														'id' => 1000135207, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Lutron
														'id' => 999959769, 
														'compatibilityImg' => "/img/LutronCaseta/Compatible-Items.png", 
														'compatibility' =>  array(
															'id' => 999969270,
															'name' => "ecobee3 Thermostat",
															'desc' => "Wi-Fi thermostat with room sensors automatically adjusts temperature according to your preferences and your home's energy use, and can be controlled from anywhere.",
															'href' => "/smartspot/pd/999969270"
														),
														array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "This learning thermostat adjusts temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)
													)
							),
		'comfort' => array(  'category_name' 	=> 'comfort',
		'product_ids'							=>  array(
														//Google Home
														'id' => 1000178395, 
														'compatibilityImg' => "/img/GoogleHome/Compatible-Items.png", 
														'compatibility' => array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "A learning thermostat that can adjust temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)	
													),
													array(
														//Tile Mate
														'id' => 1000135231, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Click and Grow
														'id' => 1000147027, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Chamberlain
														'id' => 999908195, 
														'compatibilityImg' => "/img/Chamberlain/Compatible-Items.png", 
														'compatibility' =>  array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "A learning thermostat that can adjust temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)
													),
													array(
														//Iris Switch
														'id' => 1000135207, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													),
													array(
														//Sylvania Lightify
														'id' => 999957973, 
														'compatibilityImg' => "/img/SylvaniaLightify/Compatible-Items.png", 
														'compatibility' => array(
															'id' => 1000125089,
															'name' => "Nest Copper Thermostat",
															'desc' => "A learning thermostat that can adjust temperatures based on a schedule and can be controlled from a smartphone.",
															'href' => "/smartspot/pd/1000125089"
														)
													),
													array(
														//Nest Copper
														'id' => 1000125089, 
														'compatibilityImg' => "/img/NestCopper/Compatible-Items.jpg", 
														'compatibility' => array(
															'id' => 999959769,
															'name' => "Lutron Caseta Home Automation Smart Kit",
															'desc' => "Control your lights, shades, and temperature from anywhere with your smartphone.",
															'href' => "/smartspot/pd/999959769"
														),
														array(
															'id' => 999908195,
															'name' => 'Chamberlain MyQ Garage Door Controller',
															'desc' => 'Control and monitor your garage door from anywhere with your smartphone.',
															'href' => "/smartspot/pd/999908195"
														),
														array(
															'id' => 50241011,
															'name' => "Kwikset Kevo SmartKey Deadbolt",
															'desc' => "Bluetooth-enabled deadbolt provides enhanced home security and keyless access right from your smartphone.",
															'href' => "/smartspot/pd/50241011"
														)
													),
													array(
														//Ecobee
														'id' => 999969270, 
														'compatibilityImg' => "", 
														'compatibility' => array()
													)
							)
	)
*/
?>