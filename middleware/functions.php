<?php
	
	/************************
	 *		PRETTY PRINT	*
	 ************************/
	
	function pa( $array, $die = true ) {
		echo "<pre>"; print_r( $array ); echo "</pre>";
		if ($die == true)
			die;
	}
	
	/************************
	 *		ZIP IN			*
	 ************************/
	
	function zipInToStore( $query ) {
		
		$ch = curl_init();
		
		$header = array();
		$header[] = 'Authorization: Basic QWRvYmU6ZW9pdWV3ZjA5ZmV3bw==';
		
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
		curl_setopt( $ch, CURLOPT_URL, "http://api.lowes.com/store/location?query=". $query ."&maxResults=5&api_key=wvgx45svg62m2g5dhqcjac2s" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_TCP_KEEPALIVE, true );
		curl_setopt( $ch, CURLOPT_TCP_KEEPIDLE, 2 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		
		$receive = curl_exec( $ch );
		
		if( curl_errno( $ch ) )
			throw new Exception( curl_error( $ch ) );
		
		curl_close( $ch );
		
		return $receive;
	
	} 
	
	/************************
	 *	PRODUCT RATINGS		*
	 ************************/
	 
	function productRating( $product_rating = 0 ) {
		if( $product_rating == 0 ):
			return "000";
			
		
		elseif ( $product_rating >= 0 && $product_rating < 0.25 ):
			return "015";
			
		elseif ( $product_rating >= 0.25 && $product_rating < 0.5 ):
			return "025";
			
		elseif ( $product_rating >= 0.5 && $product_rating < 0.75 ):
			return "050";
			
		elseif ( $product_rating >= 0.75 && $product_rating < 1 ):
			return "075";
		
			
		elseif( $product_rating == 1 ):
			return "100";
			
			
		elseif ( $product_rating > 1 && $product_rating < 1.25 ):
			return "115";
			
		elseif ( $product_rating >= 1.25 && $product_rating < 1.5 ):
			return "125";
			
		elseif ( $product_rating >= 1.5 && $product_rating < 1.75 ):
			return "150";
			
		elseif ( $product_rating >= 1.75 && $product_rating < 2 ):
			return "175";
		
			
		elseif( $product_rating == 2 ):
			return "200";
			
		elseif ( $product_rating > 2 && $product_rating < 2.25 ):
			return "215";
			
		elseif ( $product_rating >= 2.25 && $product_rating < 2.5 ):
			return "225";
			
		elseif ( $product_rating >= 2.5 && $product_rating < 2.75 ):
			return "250";
			
		elseif ( $product_rating >= 2.75 && $product_rating < 3 ):
			return "275";
		
			
		elseif( $product_rating == 3 ):
			return "300";
			
		elseif ( $product_rating > 3 && $product_rating < 3.25 ):
			return "315";
			
		elseif ( $product_rating >= 3.25 && $product_rating < 3.5 ):
			return "325";
			
		elseif ( $product_rating >= 3.5 && $product_rating < 3.75 ):
			return "350";
			
		elseif ( $product_rating >= 3.75 && $product_rating < 4 ):
			return "375";
		
			
		elseif( $product_rating == 4 ):
			return "400";
			
		elseif ( $product_rating > 4 && $product_rating < 4.25 ):
			return "415";
			
		elseif ( $product_rating >= 4.25 && $product_rating < 4.5 ):
			return "425";
			
		elseif ( $product_rating >= 4.5 && $product_rating < 4.75 ):
			return "450";
			
		elseif ( $product_rating >= 4.75 && $product_rating < 5 ):
			return "475";
		
			
		elseif( $product_rating == 0 ):
			return "500";
		
		endif;
	}
	
	/************************
	 *	SERVER LOAD			*
	 ************************/
	 
	 function get_server_load() {
    
        if (stristr(PHP_OS, 'win')) {
        
            $wmi = new COM("Winmgmts://");
            $server = $wmi->execquery("SELECT LoadPercentage FROM Win32_Processor");
            
            $cpu_num = 0;
            $load_total = 0;
            
            foreach($server as $cpu){
                $cpu_num++;
                $load_total += $cpu->loadpercentage;
            }
            
            $load = round($load_total/$cpu_num);
            
        } else {
        
            $sys_load = sys_getloadavg();
            $load = $sys_load[0];
        
        }
        
        return (int) $load;
    
    }
	 
	/************************
	 *	RANDOM PASSWORD		*
	 ************************/
	 
	function createRandomPassword( $length, $strength=8 ) {
	    
	    $vowels = 'aeuy';
	    $consonants = 'bdghjmnpqrstvz';
	    
	    if ($strength >= 1)
	        $consonants .= 'BDGHJLMNPQRSTVWXZ';
	        
	    if ($strength >= 2)
	        $vowels .= "AEUY";
	    
	    if ($strength >= 4)
	        $consonants .= '23456789';
	    
	    if ($strength >= 8)
	        $consonants .= '@#$%';
	    
	    $password = '';
	    $alt = time() % 2;
	    
	    for ( $i = 0; $i < $length; $i++ ):
	        if ($alt == 1):
	            $password .= $consonants[(rand() % strlen($consonants))];
	            $alt = 0;
	        else:
	            $password .= $vowels[(rand() % strlen($vowels))];
	            $alt = 1;
	        endif;
	    endfor;
	    
	    return $password;
	}
	
	/************************
	 *	NEW CMS USER		*
	 ************************/
	 
	function createNewUser( $username, $fullname, $email, $permission, $pdo ) {
    	
    	if( $username != '' && $fullname != '' && $email != '' && $permission != '' ):
			$userexists = 	$pdo->select()
								->from( 'liftoff_core_users' )
								->where( 'username', 'LIKE', $username )
								->execute()
								->fetch();
			
			if( empty( $userexists ) ):
			
				$emailexists = 	$pdo->select()
									->from( 'liftoff_core_users' )
									->where( 'emailaddress', 'LIKE', $email )
									->execute()
									->fetch();
				
				if( empty( $emailexists ) ):
				
					$password_readable = createRandomPassword( 6, 8 );
					$password_hashed = hash( 'sha256', $password_readable ); 
					
					$pdo->insert( array( 'id', 'username', 'password', 'fullname', 'emailaddress', 'tel', 'jobtitle', 'building', 'booth', 'authorized', 'permission_level', 'last_login', 'first_login' ) )
						->into('liftoff_core_users')
						->values( array( '', $username, $password_hashed, $fullname, $email, '', '', '', '', 'yes', $permission, 0, 'yes' ) )
						->execute();
					
					$mail = new PHPMailer;

				    $mail->setFrom( 'account@liftoff.lowes.com', 'LiftOff CMS' );
				    $mail->addAddress( $email );
				    $mail->addReplyTo( 'no-reply@liftoff.lowes.com', 'LiftOff CMS' );
				
				    $mail->isHTML(true);
				
				    $mail->Subject = 'Your new LiftOff CMS credentials';
				
					include "includes/new-user-email-template.php"; //Contains email template
				    
				    $mail->Body    = str_replace( array( '[[FULLNAME]]', '[[USERNAME]]', '[[PASSWORD]]' ), array( $fullname, $username, $password_readable ), $email_template );
				
				    if( $mail->send() ):
				    
				    	unset( $_SESSION['newuser'] );
				    	
						return json_encode( array('type' => 'success', 'alertclass' => 'success', 'msg' => '<strong>Success:</strong> The user was created and the login email sent.') );
				   
				    else:
				    
				    	return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> The email could not be sent.') );
				    	
				    endif;
					
				else:
				
					return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> The address '.$email.' already exists.') );
				
				endif;
			
			else:
			
				return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> The username '.$username.' already exists.') );
				
			endif;
			
		else:
		
			return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> You left a blank field, all fields are required.') );
		
		endif;
		
    }
	
	/************************
	 *	DELETE CMS USER		*
	 ************************/
	 
	function deleteUser( $userid, $pdo ) {
		
		$pdo->delete()
			->from( 'liftoff_core_users' )
			->where( 'id', '=', $userid )
			->execute();
		
		return json_encode( array('type' => 'success', 'alertclass' => 'success', 'msg' => '<strong>Success:</strong> The user was permanently deleted from the system.') );
	}	 
	
	/************************
	 *	CREATE NEW SITE		*
	 ************************/
	
	function createNewSite( $sitename, $description, $status, $time, $pdo ) {
    	
    	if( $sitename != '' && $description != '' && $status != '' ):
			
			$pdo->insert( array( 'id', 'site_name', 'description', 'published_date', 'status' ) )
				->into('liftoff_core_sites')
				->values( array( '', $sitename, $description, $time, $status ) )
				->execute();
			
			return json_encode( array('type' => 'success', 'alertclass' => 'success', 'msg' => '<strong>Success:</strong> The new site was created.') );
			
		else:
		
			return json_encode( array('type' => 'error', 'alertclass' => 'danger', 'msg' => '<strong>Error:</strong> You left a blank field, all fields are required.') );
		
		endif;
		
    }
	 
	
	/************************
	 *	SET SITE STATUS		*
	 ************************/
	 
	function setSiteStatus( $siteid, $status, $pdo ) {
		
		$pdo->update( array( 'status' => $status ) )
            ->table( 'liftoff_core_sites' )
            ->where( 'id', '=', $siteid )
			->execute();
		
		return json_encode( array('type' => 'success', 'alertclass' => 'success', 'msg' => '<strong>Success:</strong> The site status was updated.') );
	}