<?php
	
	if( file_exists( __DIR__.'/environment.php' ) ):
	
		/* FOR LOCAL DEVELOPMENT, CREATE /dependencies/environment.php with "<?php $environment = 'local'; ?>" inside. */
		include( __DIR__.'/environment.php' );
	else:
		$environment = 'dev';
	endif;
	
	if ( $environment == 'local' ):
	    $config = [
	        'settings' => [
	            'determineRouteBeforeAppMiddleware' => false,
	            'displayErrorDetails' => true,
	            'db' => [
	                'driver' 	=> 'mysql',
	                'host' 		=> 'localhost',
	                'database' 	=> 'smarthome',
	                'username' 	=> 'root',
	                'password' 	=> 'root',
	                'charset'   => 'utf8',
	                'collation' => 'utf8_bin',
	                'prefix'    => '',
	                'dsn'		=> 'mysql:host=localhost;dbname=smarthome;charset=utf8'
	            ]
	        ],
	    ];
	elseif ( $environment == 'dev' ):
	    $config = [
	        'settings' => [
	            'determineRouteBeforeAppMiddleware' => false,
	            'displayErrorDetails' => true,
	            'db' => [
	                'driver' 	=> 'mysql',
	                'host' 		=> '50.62.56.168',
	                'database' 	=> 'robmilne_smartspot',
	                'username' 	=> 'robmilne_smart',
	                'password' 	=> 'SM4RTH0M3#',
	                'charset'   => 'utf8',
	                'collation' => 'utf8_bin',
	                'prefix'    => '',
	                'dsn'		=> 'mysql:host=50.62.56.168;dbname=robmilne_smartspot;charset=utf8'
	            ]
	        ],
	    ];
	endif;

?>